// Minimum eight characters, at least one letter, one number and one special character
export const CHECK_PASSWORD = new RegExp(
  `^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$`,
);

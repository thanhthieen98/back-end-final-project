export const VERIFY_MAIL_TEMPLATE = `<!DOCTYPE html>
  <html lang="en">
  <body class="container">
    <div class="jumbotron">
    <p>Please verify your email by clicking this link: <a href="<%= emailVerifyLink %>" role="button">Verify Email</a></p>
      <i>Note: This link invalid in next 20 minutes!</i>
    </div>
  </body>
  </html>`;

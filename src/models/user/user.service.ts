import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateDto } from './dto/update.dto';
import { UserEntity } from './dto/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async create(data: Partial<UserEntity>): Promise<UserEntity> {
    return this.userRepository.save(new UserEntity(data));
  }

  async createUserGoogle(user): Promise<UserEntity> {
    return await this.userRepository.save({
      id_google: user.id,
      email: user.email,
      name: user.name,
      code: 'SET_UP_PASSWORD',
    });
  }

  async findOne(email: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({ email });

    if (!user) {
      return null;
    }
    delete user.id_google;
    return user;
  }

  async getProfile(email: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({ email });

    if (!user) {
      return null;
    }
    delete user.id;
    delete user.password;
    delete user.id_google;
    delete user.code;
    return user;
  }

  async update(id: number, updates: UpdateUserDto): Promise<UserEntity> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException('Người dùng không tồn tại');
    }

    Object.assign(user, updates);
    return await this.userRepository.save(user);
  }

  async updateUser(id: number, updates: UpdateDto): Promise<UserEntity> {
    const user = await this.userRepository.findOne(id);
    if (!user) {
      throw new NotFoundException('Người dùng không tồn tại');
    }
    delete user.password;
    Object.assign(user, updates);

    return await this.userRepository.save(user);
  }

  async updatePassword(id: number, password: string): Promise<any> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException('Người dùng không tồn tại');
    }

    return await this.userRepository.save({ ...user, password });
  }

  async updatePasswordGoogle(id: number, password: string): Promise<any> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException('Người dùng không tồn tại');
    }

    return await this.userRepository.save({ ...user, password, code: 'DONE' });
  }

  async findAndUpdateUserGoogle(user: any): Promise<UserEntity> {
    const userRepo = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
    });

    return await this.userRepository.save({
      ...userRepo,
      id_google: user.id,
      code: 'SET_UP_PASSWORD',
    });
  }
}

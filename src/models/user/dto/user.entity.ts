import { ApiProperty } from '@nestjs/swagger';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

import * as bcrypt from 'bcrypt';
import { CHECK_PASSWORD } from 'src/common/regex';
import { Exclude } from 'class-transformer';
import { BadRequestException } from '@nestjs/common';

@Entity({ name: 'user' })
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @ApiProperty()
  name: string;

  @Column({ nullable: true })
  id_google: string;

  @Column({ unique: true })
  @ApiProperty()
  email: string;

  @Column({ nullable: true })
  @ApiProperty({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  @ApiProperty({ nullable: true })
  address: string;

  @Column({ nullable: true })
  @ApiProperty({ nullable: true })
  gender: string;

  @Column({ nullable: true })
  @ApiProperty()
  @Exclude()
  password: string;

  @Column({ nullable: true })
  @ApiProperty()
  code: string;

  constructor(data: Partial<UserEntity> = {}) {
    Object.assign(this, data);
  }

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    const salt = await bcrypt.genSalt();
    if (this.password) {
      this.password = await bcrypt.hash(this.password, salt);
    }
  }

  async checkPassword(plainPassword: string): Promise<boolean> {
    return await bcrypt.compare(plainPassword, this.password);
  }
}

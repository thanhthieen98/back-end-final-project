import { IsDefined, IsString, IsNotEmpty } from 'class-validator';
export class CreateUserDto {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly email: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly phone: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly address: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly password: string;
}

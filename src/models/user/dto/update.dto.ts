import { IsDefined, IsString, IsNotEmpty } from 'class-validator';
export class UpdateDto {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly phone: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly address: string;
}

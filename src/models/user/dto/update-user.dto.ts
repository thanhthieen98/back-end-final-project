import { IsDefined, IsString, IsNotEmpty } from 'class-validator';
export class UpdateUserDto {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly phone: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly address: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly password: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly id_google: string;

  @IsString()
  readonly code?: string;
}

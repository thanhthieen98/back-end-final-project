import { ApiProperty } from '@nestjs/swagger';
import {
  IsDefined,
  IsEmail,
  IsEmpty,
  IsNotEmpty,
  IsPhoneNumber,
  Matches,
  MinLength,
} from 'class-validator';

export class SignUpDto {
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsEmail()
  @ApiProperty()
  email: string;

  @IsDefined()
  @IsPhoneNumber('VN')
  @ApiProperty({ nullable: true })
  phone: string;

  @IsDefined()
  @ApiProperty({ nullable: true })
  address: string;

  @IsNotEmpty()
  @MinLength(8, {
    message: 'Mật khẩu ít nhất phải có 8 ký tự',
  })
  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, {
    message: 'Mật khẩu không đúng định dạng',
  })
  @ApiProperty({
    minLength: 8,
  })
  password: string;
}

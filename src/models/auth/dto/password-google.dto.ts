import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Matches, MinLength } from 'class-validator';

export class PasswordGoogleDto {
  @IsNotEmpty()
  @MinLength(8, {
    message: 'Mật khẩu ít nhất phải có 8 ký tự',
  })
  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, {
    message: 'Mật khẩu không đúng định dạng',
  })
  @ApiProperty()
  readonly password: string;
}

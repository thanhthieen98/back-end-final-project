import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsNotEmpty } from 'class-validator';

export class SendMailResetDto {
  @IsDefined()
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  readonly email: string;
}

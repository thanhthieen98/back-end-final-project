import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNotEmpty, Matches, MinLength } from 'class-validator';

export class ChangePasswordDto {
  @IsNotEmpty()
  @ApiProperty()
  readonly password: string;
  @IsNotEmpty()
  @MinLength(8, {
    message: 'Mật khẩu ít nhất phải có 8 ký tự',
  })
  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, {
    message: 'Mật khẩu không đúng định dạng',
  })
  @ApiProperty()
  readonly newPassword: string;
}

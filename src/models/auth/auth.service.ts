import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import * as ejs from 'ejs';
import * as nodemailer from 'nodemailer';
import { VERIFY_MAIL_TEMPLATE } from 'src/common/templates/verifyMail';
import { MAIL_SERVER } from 'src/config/mailServer';
import { UserEntity } from '../user/dto/user.entity';
import { UserService } from '../user/user.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { SignUpDto } from './dto/sign-up.dto';
import * as SendGrid from '@sendgrid/mail';
import { config } from 'dotenv';
import { OAuth2Client } from 'google-auth-library';
import { PasswordGoogleDto } from './dto/password-google.dto';

config();

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {
    SendGrid.setApiKey(process.env.SEND_GRID_KEY);
  }

  async validateUser(email: string, pass: string): Promise<UserEntity | null> {
    const user = await this.userService.findOne(email);
    if (user && (await user.checkPassword(pass))) {
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = { email: user.email, sub: user.id };
    delete user.password;
    return {
      access_token: this.jwtService.sign(payload),
      user: user,
    };
  }

  async register(signUp: SignUpDto): Promise<UserEntity> {
    const user = await this.userService.create(signUp);
    delete user.password;
    return user;
  }

  googleAuth = async (token) => {
    const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.GOOGLE_CLIENT_ID,
    });
    const payload = ticket.getPayload();
    const { sub, email, family_name, given_name } = payload;
    return {
      email,
      id: sub,
      name: family_name + ' ' + given_name,
    };
  };

  async googleLogin(req) {
    const user = await this.googleAuth(req.user.token);
    if (!user) {
      return new NotFoundException('Người dùng không tồn tại');
    }
    let userRepo = await this.userService.findOne(user.email);

    if (!userRepo) {
      userRepo = await this.userService.createUserGoogle(user);
    }

    const payload = { email: userRepo.email, sub: userRepo.id };

    return {
      user: user,
      code: userRepo.code,
      access_token: this.jwtService.sign(payload),
    };

    // return await this.googleAuth(req.user.token);
  }

  async changePassword(body: ChangePasswordDto, user: any) {
    const userRepo = await this.validateUser(user.email, body.password);
    if (!userRepo) {
      throw new NotFoundException('Mât khẩu không đúng!');
    }
    const salt = await bcrypt.genSalt();
    const newPassword = await bcrypt.hash(body.newPassword, salt);
    const userRepoUpdate = await this.userService.updatePassword(
      user.userId,
      newPassword,
    );

    if (!userRepoUpdate)
      throw new InternalServerErrorException('Đã có lỗi xảy ra!');

    return { message: 'Thay đổi mật khẩu thành công' };
  }

  async setupPasswordGoogle(body: PasswordGoogleDto, user: any) {
    const userRepo = await this.userService.findOne(user.email);
    if (!userRepo || userRepo.code !== 'SET_UP_PASSWORD') {
      throw new NotFoundException('Mât khẩu không đúng!');
    }
    const salt = await bcrypt.genSalt();
    const newPassword = await bcrypt.hash(body.password, salt);
    const userRepoUpdate = await this.userService.updatePasswordGoogle(
      user.userId,
      newPassword,
    );

    if (!userRepoUpdate)
      throw new InternalServerErrorException('Đã có lỗi xảy ra!');

    return { message: 'Thay đổi mật khẩu thành công' };
  }

  async sendMail(email: string) {
    try {
      const verifyLink = `http://localhost:8000/api`;
      const htmlContent = ejs.render(VERIFY_MAIL_TEMPLATE, {
        emailVerifyLink: verifyLink,
      });
      const option = {
        from: MAIL_SERVER.auth.user,
        to: email,
        subject: 'Reset password',
        html: htmlContent,
      };
      const transporter = nodemailer.createTransport(MAIL_SERVER);
      await transporter.sendMail(option);
      return {
        message: 'Mật khẩu đã được gửi về mail của bạn. Hãy kiểm tra!',
      };
    } catch (error) {
      throw new InternalServerErrorException('Đã có lỗi xảy ra');
    }
  }

  async send(mail: SendGrid.MailDataRequired) {
    try {
      await SendGrid.send(mail);
      return {
        message:
          'Một thông báo đã được gửi đến mail của bạn, Vui lòng kiểm tra!',
      };
    } catch (error) {
      throw new InternalServerErrorException('Đã có lỗi xảy ra');
    }
  }
}

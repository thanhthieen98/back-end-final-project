import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiTags,
} from '@nestjs/swagger';
import * as ejs from 'ejs';
import { JwtAuthGuard, LocalAuthGuard } from 'src/common/guards';
import { VERIFY_MAIL_TEMPLATE } from 'src/common/templates/verifyMail';
import { AuthService } from './auth.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { PasswordGoogleDto } from './dto/password-google.dto';
import { SendMailResetDto } from './dto/send-mail-reset.dto';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @ApiBody({ type: SignInDto })
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
  })
  register(@Body() signUp: SignUpDto): any {
    return this.authService.register(signUp);
  }

  @Post('login-google')
  // @UseGuards(GoogleAuthGuard)
  async googleAuthRedirect(@Body() body) {
    return await this.authService.googleLogin(body);
  }

  // @Get('google')
  // @UseGuards(GoogleAuthGuard)
  // async googleAuth() {
  //   return null;
  // }

  @Post('change-password')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiBody({ type: ChangePasswordDto })
  async changePassword(
    @Request() req,
    @Body() body: ChangePasswordDto,
  ): Promise<any> {
    return await this.authService.changePassword(body, req.user);
  }

  @Post('setup-password-google')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  async setUpPasswordGoogle(
    @Request() req,
    @Body() body: PasswordGoogleDto,
  ): Promise<any> {
    return await this.authService.setupPasswordGoogle(body, req.user);
  }

  @Post('send-mail-reset-test')
  async sendMailResetTest(@Body() body: SendMailResetDto) {
    await this.authService.sendMail(body.email);
  }

  @Post('send-mail-reset')
  async sendMailReset(@Body() body: SendMailResetDto) {
    const verifyLink = `http://localhost:8000/api`;

    const htmlContent = ejs.render(VERIFY_MAIL_TEMPLATE, {
      emailVerifyLink: verifyLink,
    });
    const mail = {
      to: body.email,
      subject: 'Reset password',
      from: 'thien98.dev@gmail.com',
      html: htmlContent,
    };
    await this.authService.send(mail);
  }
}

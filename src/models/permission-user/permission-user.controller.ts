import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { PermissionUserEntity } from './dto/permission-user.entity';
import { PermissionUserService } from './permission-user.service';

@Crud({
  model: { type: PermissionUserEntity },
})
@Controller('permission-user')
export class PermissionUserController
  implements CrudController<PermissionUserEntity>
{
  constructor(public service: PermissionUserService) {}
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PermissionUserEntity } from './dto/permission-user.entity';

@Injectable()
export class PermissionUserService extends TypeOrmCrudService<PermissionUserEntity> {
  constructor(@InjectRepository(PermissionUserEntity) repo) {
    super(repo);
  }
}

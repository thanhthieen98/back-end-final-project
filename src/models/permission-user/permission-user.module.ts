import { Module } from '@nestjs/common';
import { PermissionUserService } from './permission-user.service';
import { PermissionUserController } from './permission-user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionUserEntity } from './dto/permission-user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PermissionUserEntity])],
  providers: [PermissionUserService],
  controllers: [PermissionUserController],
  exports: [PermissionUserService],
})
export class PermissionUserModule {}

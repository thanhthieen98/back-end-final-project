import { ApiProperty } from '@nestjs/swagger';
import { AdminEntity } from 'src/models/admin/dto/admin.entity';
import { PermissionEntity } from 'src/models/permission/dto/permission.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'permission_user' })
export class PermissionUserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'enum', enum: [1 | 0] })
  @ApiProperty({ type: Number, enum: [1, 0] })
  licensed: 1 | 0;

  @OneToOne(() => PermissionEntity, { eager: true })
  @JoinColumn({ name: 'permissionId' })
  permission: PermissionEntity;

  @Column()
  @ApiProperty()
  permissionId: number;

  @OneToOne(() => AdminEntity, { eager: true })
  @JoinColumn({ name: 'adminId' })
  admin: AdminEntity;

  @Column()
  @ApiProperty()
  adminId: number;
}

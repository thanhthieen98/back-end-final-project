import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { AdminEntity } from './dto/admin.entity';

@Injectable()
export class AdminService extends TypeOrmCrudService<AdminEntity> {
  constructor(@InjectRepository(AdminEntity) repo) {
    super(repo);
  }
}

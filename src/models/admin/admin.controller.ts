import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { AdminService } from './admin.service';
import { AdminEntity } from './dto/admin.entity';

@Crud({
  model: {
    type: AdminEntity,
  },
})
@ApiTags('admin')
@Controller('admin')
export class AdminController implements CrudController<AdminEntity> {
  constructor(public service: AdminService) {}
}

import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { CategoryService } from './category.service';
import { CategoryEntity } from './dto/category.entity';

@Crud({
  model: {
    type: CategoryEntity,
  },
})
@ApiTags('category')
@Controller('category')
export class CategoryController implements CrudController<CategoryEntity> {
  constructor(public service: CategoryService) {}
}

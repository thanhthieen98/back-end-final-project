import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { PermissionEntity } from './dto/permission.entity';
import { PermissionService } from './permission.service';

@Crud({
  model: {
    type: PermissionEntity,
  },
})
@ApiTags('permission')
@Controller('permission')
export class PermissionController implements CrudController<PermissionEntity> {
  constructor(public service: PermissionService) {}
}

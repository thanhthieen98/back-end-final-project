import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PermissionEntity } from './dto/permission.entity';

@Injectable()
export class PermissionService extends TypeOrmCrudService<PermissionEntity> {
  constructor(@InjectRepository(PermissionEntity) repo) {
    super(repo);
  }
}

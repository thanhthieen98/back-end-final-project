import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PermissionDetailEntity } from './dto/permission-detail.entity';

@Injectable()
export class PermissionDetailService extends TypeOrmCrudService<PermissionDetailEntity> {
  constructor(@InjectRepository(PermissionDetailEntity) repo) {
    super(repo);
  }
}

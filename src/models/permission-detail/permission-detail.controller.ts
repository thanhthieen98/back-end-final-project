import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { PermissionDetailEntity } from './dto/permission-detail.entity';
import { PermissionDetailService } from './permission-detail.service';

@Crud({
  model: {
    type: PermissionDetailEntity,
  },
})
@ApiTags('permission-detail')
@Controller('permission-detail')
export class PermissionDetailController
  implements CrudController<PermissionDetailEntity>
{
  constructor(public service: PermissionDetailService) {}
}

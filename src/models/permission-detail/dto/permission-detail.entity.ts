import { ApiProperty } from '@nestjs/swagger';
import { PermissionEntity } from 'src/models/permission/dto/permission.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

export type role = 'CREATE' | 'EDIT' | 'DELETE' | 'VIEW';

@Entity({ name: 'permission_detail' })
export class PermissionDetailEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'enum', enum: ['CREATE', 'EDIT', 'DELETE', 'VIEW'] })
  @ApiProperty({ enum: ['CREATE', 'EDIT', 'DELETE', 'VIEW'] })
  code: role;

  @Column({ type: 'enum', enum: [1, 0] })
  @ApiProperty({ type: Number, enum: [1, 0] })
  checkAction: 1 | 0;

  @ManyToOne(() => PermissionEntity)
  @JoinColumn({
    name: 'permissionId',
  })
  permission: PermissionEntity;

  @Column()
  @ApiProperty()
  permissionId: number;
}

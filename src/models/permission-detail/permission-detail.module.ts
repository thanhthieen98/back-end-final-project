import { Module } from '@nestjs/common';
import { PermissionDetailService } from './permission-detail.service';
import { PermissionDetailController } from './permission-detail.controller';

import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionDetailEntity } from './dto/permission-detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PermissionDetailEntity])],
  providers: [PermissionDetailService],
  controllers: [PermissionDetailController],
  exports: [PermissionDetailService],
})
export class PermissionDetailModule {}

import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { NewsEntity } from './dto/news.entity';
import { NewsService } from './news.service';

@Crud({
  model: {
    type: NewsEntity,
  },
})
@ApiTags('news')
@Controller('news')
export class NewsController implements CrudController<NewsEntity> {
  constructor(public service: NewsService) {}
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { NewsEntity } from './dto/news.entity';

@Injectable()
export class NewsService extends TypeOrmCrudService<NewsEntity> {
  constructor(@InjectRepository(NewsEntity) repo) {
    super(repo);
  }
}

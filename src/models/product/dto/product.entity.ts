import { ApiProperty } from '@nestjs/swagger';
import { CategoryEntity } from 'src/models/category/dto/category.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({
  name: 'product',
})
export class ProductEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @ApiProperty()
  name: string;

  @Column()
  @ApiProperty()
  price: number;

  @Column({ nullable: true })
  @ApiProperty({ nullable: true })
  image: string;

  @ManyToOne(() => CategoryEntity, { cascade: true })
  @JoinColumn({
    name: 'categoryId',
  })
  category: CategoryEntity;

  @Column()
  @ApiProperty()
  categoryId: number;
}

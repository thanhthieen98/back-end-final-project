import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { ProductEntity } from './dto/product.entity';
import { ProductService } from './product.service';

@Crud({
  model: {
    type: ProductEntity,
  },
  query: {
    join: {
      category: {
        eager: true,
      },
    },
  },
})
@ApiTags('product')
@Controller('product')
export class ProductController implements CrudController<ProductEntity> {
  constructor(public service: ProductService) {}
}

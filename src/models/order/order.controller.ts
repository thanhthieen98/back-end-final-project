import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { OrderEntity } from './dto/order.entity';
import { OrderService } from './order.service';

@Crud({ model: { type: OrderEntity } })
@ApiTags('order')
@Controller('order')
export class OrderController implements CrudController<OrderEntity> {
  constructor(public service: OrderService) {}
}

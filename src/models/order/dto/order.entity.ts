import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'order',
})
export class OrderEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  code: string;
}

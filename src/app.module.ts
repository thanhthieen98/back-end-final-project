import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnectionOptions } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  CategoryModule,
  ProductModule,
  AdminModule,
  NewsModule,
  OrderModule,
  PermissionDetailModule,
  PermissionModule,
  PermissionUserModule,
  UserModule,
  AuthModule,
} from './models';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: async () =>
        Object.assign(await getConnectionOptions(), {
          autoLoadEntities: true,
        }),
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ProductModule,
    CategoryModule,
    AdminModule,
    NewsModule,
    OrderModule,
    PermissionDetailModule,
    PermissionModule,
    PermissionUserModule,
    UserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
